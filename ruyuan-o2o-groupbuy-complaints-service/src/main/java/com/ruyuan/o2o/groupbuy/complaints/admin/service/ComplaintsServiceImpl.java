package com.ruyuan.o2o.groupbuy.complaints.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.complaints.admin.dao.ComplaintsDao;
import com.ruyuan.o2o.groupbuy.complaints.model.ComplaintsModel;
import com.ruyuan.o2o.groupbuy.complaints.service.ComplaintsService;
import com.ruyuan.o2o.groupbuy.complaints.vo.ComplaintsVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理投诉服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ComplaintsService.class, cluster = "failfast", loadbalance = "roundrobin")
public class ComplaintsServiceImpl implements ComplaintsService {

    @Autowired
    private ComplaintsDao complaintsDao;

    /**
     * 接收投诉
     *
     * @param complaintsVO
     */
    @Override
    public void save(ComplaintsVO complaintsVO) {
        ComplaintsModel complaintsModel = new ComplaintsModel();
        BeanMapper.copy(complaintsVO, complaintsModel);
        complaintsModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        complaintsDao.save(complaintsModel);
    }
}
