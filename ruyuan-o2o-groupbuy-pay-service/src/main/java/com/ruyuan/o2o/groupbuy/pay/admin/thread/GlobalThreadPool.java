package com.ruyuan.o2o.groupbuy.pay.admin.thread;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

/**
 * 系统线程池
 *
 * @author ming qian
 */
@Configuration
public class GlobalThreadPool {

    ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            2,
            4,
            60,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(200),
            new ThreadFactoryBuilder().setNameFormat("message-pool-%d").build(), new ThreadPoolExecutor.AbortPolicy());

    @Bean
    public ThreadPoolExecutor getThreadPool() {
        return poolExecutor;
    }
}
