package com.ruyuan.o2o.groupbuy.coupons.admin.controller;

import com.ruyuan.o2o.groupbuy.coupons.service.CouponsReceiveService;
import com.ruyuan.o2o.groupbuy.coupons.service.CouponsService;
import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsReceiveVO;
import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户领取优惠券管理
 * 模拟用户在实际门店中，手动扫码领取优惠券
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/coupons")
@Slf4j
@Api(tags = "用户领取优惠券管理")
public class CouponsReceiveController {

    @Reference(version = "1.0.0", interfaceClass = CouponsReceiveService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private CouponsReceiveService couponsReceiveService;

    /**
     * 用户领取优惠券
     *
     * @param couponsReceiveVO 前台表单
     * @return
     */
    @PostMapping("/receive")
    @ApiOperation("用户领取优惠券")
    public String receiveCoupons(@RequestBody CouponsReceiveVO couponsReceiveVO) {
        if (!couponsReceiveService.receiveCoupons(couponsReceiveVO)) {
            return "failed";
        }
        log.info("领取优惠券:{} 完成", couponsReceiveVO.getCouponsId());
        return "success";
    }
}
