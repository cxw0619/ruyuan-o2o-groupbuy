package com.ruyuan.o2o.groupbuy.coupons.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.coupons.admin.dao.CouponsDao;
import com.ruyuan.o2o.groupbuy.coupons.admin.dao.CouponsReceiveDao;
import com.ruyuan.o2o.groupbuy.coupons.model.CouponsModel;
import com.ruyuan.o2o.groupbuy.coupons.model.CouponsReceiveModel;
import com.ruyuan.o2o.groupbuy.coupons.service.CouponsService;
import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理优惠券服务的service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = CouponsService.class, cluster = "failfast", loadbalance = "roundrobin")
public class CouponsServiceImpl implements CouponsService {

    @Autowired
    private CouponsDao couponsDao;

    /**
     * 保存优惠券
     *
     * @param couponsVO
     */
    @Override
    public Boolean save(CouponsVO couponsVO) {
        CouponsModel couponsModel = new CouponsModel();
        BeanMapper.copy(couponsVO, couponsModel);
        couponsModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        couponsModel.setCouponStartTime(TimeUtil.format(System.currentTimeMillis()));
        couponsModel.setCouponEndTime(TimeUtil.format(System.currentTimeMillis()));
        couponsDao.save(couponsModel);
        return Boolean.TRUE;
    }

    /**
     * 分页查询优惠券
     *
     * @return
     */
    @Override
    public List<CouponsVO> listByPage(Integer pageNum, Integer pageSize) {
        List<CouponsVO> couponsVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<CouponsModel> couponsModels = couponsDao.listByPage();

        for (CouponsModel couponsModel : couponsModels) {
            CouponsVO couponsVO = new CouponsVO();
            BeanMapper.copy(couponsModel, couponsVO);
            couponsVoS.add(couponsVO);
        }

        return couponsVoS;
    }
}
