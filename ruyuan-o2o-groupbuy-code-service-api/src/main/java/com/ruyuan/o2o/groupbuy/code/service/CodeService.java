package com.ruyuan.o2o.groupbuy.code.service;

import com.ruyuan.o2o.groupbuy.code.vo.CodeVO;

/**
 * 券码服务service组件接口
 *
 * @author ming qian
 */
public interface CodeService {

    /**
     * 生成券码
     *
     * @param codeVO
     */
    void save(CodeVO codeVO);

    /**
     * 根据id查询券码
     *
     * @param userId 用户id
     * @param payId  支付id
     * @return
     */
    CodeVO findById(Integer userId, Integer payId);

    /**
     * 核销券码
     *
     * @param codeVO
     */
    void writeOffCode(CodeVO codeVO);
}
