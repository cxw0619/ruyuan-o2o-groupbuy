package com.ruyuan.o2o.groupbuy.user.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.user.admin.dao.UserDao;
import com.ruyuan.o2o.groupbuy.user.model.UserModel;
import com.ruyuan.o2o.groupbuy.user.service.UserService;
import com.ruyuan.o2o.groupbuy.user.vo.UserVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理消费者用户服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = UserService.class, cluster = "failfast", loadbalance = "roundrobin")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    /**
     * 用户注册
     *
     * @param userVO
     */
    @Override
    public void save(UserVO userVO) {
        UserModel userModel = new UserModel();
        BeanMapper.copy(userVO, userModel);
        userModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        userModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        userDao.save(userModel);
    }

    /**
     * 查询用户手机号
     *
     * @param userId 消费者用户id
     * @return
     */
    @Override
    public String findPhoneById(Integer userId) {
        return userDao.findPhoneById(userId);
    }
}
