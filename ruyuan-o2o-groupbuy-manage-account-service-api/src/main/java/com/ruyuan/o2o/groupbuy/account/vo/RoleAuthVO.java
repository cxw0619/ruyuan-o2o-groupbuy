package com.ruyuan.o2o.groupbuy.account.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 角色绑定权限VO类
 *
 * @author ming qian
 */
@ApiModel(value = "角色绑定权限VO类")
@Getter
@Setter
@ToString
public class RoleAuthVO implements Serializable {
    private static final long serialVersionUID = 9022518393953603674L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private Integer roleId;

    /**
     * 权限id
     */
    @ApiModelProperty("权限id")
    private String authIds;
}
