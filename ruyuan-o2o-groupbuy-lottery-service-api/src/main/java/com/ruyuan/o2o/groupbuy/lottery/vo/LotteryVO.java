package com.ruyuan.o2o.groupbuy.lottery.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 抽奖服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "抽奖服务VO类")
@Getter
@Setter
@ToString
public class LotteryVO implements Serializable {
    private static final long serialVersionUID = -4714501692928387073L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;

    /**
     * 抽奖商品id
     */
    @ApiModelProperty("抽奖商品id")
    private Integer itemId;

    /**
     * 抽奖所在门店id
     */
    @ApiModelProperty("抽奖所在门店id")
    private Integer storeId;

    /**
     * 抽奖结果 0 未中奖 1 中奖
     */
    @ApiModelProperty("抽奖结果 0 未中奖 1 中奖")
    private Integer lotteryResult;

    /**
     * 抽奖时间
     */
    @ApiModelProperty("抽奖时间")
    private String createTime;

}