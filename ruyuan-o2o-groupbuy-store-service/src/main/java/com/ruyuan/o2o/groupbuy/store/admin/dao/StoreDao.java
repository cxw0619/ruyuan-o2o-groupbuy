package com.ruyuan.o2o.groupbuy.store.admin.dao;

import com.ruyuan.o2o.groupbuy.store.model.StoreModel;
import com.ruyuan.o2o.groupbuy.store.vo.StoreVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 门店服务DAO
 *
 * @author ming qian
 */
@Repository
public interface StoreDao {
    /**
     * 更新门店宣传标语
     *
     * @param storeModel
     */
    void update(StoreModel storeModel);

    /**
     * 分页查询门店
     *
     * @return
     */
    List<StoreModel> listByPage();

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return
     */
    StoreModel findById(Integer storeId);
}
