package com.ruyuan.o2o.groupbuy.lottery.admin.dao;

import com.ruyuan.o2o.groupbuy.lottery.model.LotteryModel;
import org.springframework.stereotype.Repository;


/**
 * 抽奖服务DAO
 *
 * @author ming qian
 */
@Repository
public interface LotteryDao {
    /**
     * 增加用户抽奖记录
     *
     * @param lotteryModel 抽奖model
     */
    void save(LotteryModel lotteryModel);

}