package com.ruyuan.o2o.groupbuy.lottery.admin.controller;

import com.ruyuan.o2o.groupbuy.lottery.service.LotteryService;
import com.ruyuan.o2o.groupbuy.lottery.vo.LotteryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 抽奖管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/lottery")
@Slf4j
@Api(tags = "抽奖管理")
public class LotteryController {

    @Reference(version = "1.0.0", interfaceClass = LotteryService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private LotteryService lotteryService;

    /**
     * 保存抽奖结果
     * 模拟只有用户实际在店核销券码消费之后可参与抽奖
     * 且奖品是随抽随拿，不支持物流配送
     *
     * @param lotteryVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("保存抽奖结果")
    public Boolean save(@RequestBody LotteryVO lotteryVO) {
        lotteryService.save(lotteryVO);
        return Boolean.TRUE;
    }

}
