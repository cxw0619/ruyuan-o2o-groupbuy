package com.ruyuan.o2o.groupbuy.complaints.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户投诉门店服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "用户投诉门店服务VO类")
@Getter
@Setter
@ToString
public class ComplaintsVO implements Serializable {
    private static final long serialVersionUID = -3355720845237384548L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 用户手机号
     */
    @ApiModelProperty("用户手机号")
    private Integer userPhone;

    /**
     * 处理状态 0 未处理 1 已回复 2 已解决 3 未解决
     */
    @ApiModelProperty("处理状态 0 未处理 1 已回复 2 已解决 3 未解决")
    private Integer processStatus;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 投诉内容
     */
    @ApiModelProperty("投诉内容")
    private String complaintsContent;

    /**
     * 解决方式
     */
    @ApiModelProperty("解决方式")
    private String processContent;
}