package com.ruyuan.o2o.groupbuy.logistics.service;


import com.ruyuan.o2o.groupbuy.logistics.vo.LogisticsVO;

/**
 * 物流配送服务service组件接口
 *
 * @author ming qian
 */
public interface LogisticsService {
    /**
     * 保存配送信息
     *
     * @param logisticsVO
     */
    void save(LogisticsVO logisticsVO);
}
