package com.ruyuan.o2o.groupbuy.item.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商品服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class ItemModel implements Serializable {
    private static final long serialVersionUID = 3402262853087693410L;

    /**
     * 商品id
     */
    private Integer itemId;

    /**
     * 所属门店id
     */
    private Integer storeId;

    /**
     * 所属商户id
     */
    private Integer shopId;

    /**
     * 商品名称
     */
    private String itemName;

    /**
     * 商品宣传描述
     */
    private String itemDesc;

    /**
     * 商品原价
     */
    private Double price;

    /**
     * 商品团购优惠价
     */
    private Double groupbuyPrice;

    /**
     * 商品销售状态 0 已创建 1 审核通过 2 审核拒绝
     */
    private Integer itemCheckStatus;

    /**
     * 商品上下架状态 0 未上架 1 已上架 2 已下架
     */
    private Integer saleStatus;

    /**
     * 商品属性 0 实物 1 虚拟
     */
    private Integer itemProperty;

    /**
     * 商品类型 0 团购商品 1 积分商品
     */
    private Integer itemType;

    /**
     * 抽奖商品中奖概率
     */
    private Double lotteryProbability;

    /**
     * 商品积分价格
     */
    private Integer pointsPrice;

    /**
     * 商品图片地址1
     */
    private String itemImg1;

    /**
     * 商品图片地址2
     */
    private String itemImg2;

    /**
     * 商品图片地址3
     */
    private String itemImg3;

    /**
     * 商品图片地址4
     */
    private String itemImg4;

    /**
     * 商品图片地址5
     */
    private String itemImg5;

    /**
     * 商品富文本宣传
     */
    private String itemRichText;

    /**
     * 商品使用规则说明
     */
    private String itemRule;

    /**
     * 商品库存
     */
    private Integer stock;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private Integer createOper;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新人
     */
    private Integer updateOper;

    /**
     * 删除标记 0 未删除 1 已删除
     */
    private Integer delFlag;
}
