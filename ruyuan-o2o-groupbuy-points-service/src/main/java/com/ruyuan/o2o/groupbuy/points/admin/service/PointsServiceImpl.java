package com.ruyuan.o2o.groupbuy.points.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.points.admin.dao.PointsDao;
import com.ruyuan.o2o.groupbuy.points.model.PointsModel;
import com.ruyuan.o2o.groupbuy.points.service.PointsService;
import com.ruyuan.o2o.groupbuy.points.vo.PointsVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理积分服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = PointsService.class, cluster = "failfast", loadbalance = "roundrobin")
public class PointsServiceImpl implements PointsService {

    @Autowired
    private PointsDao pointsDao;

    /**
     * 增加用户积分
     *
     * @param pointsVO
     */
    @Override
    public void save(PointsVO pointsVO) {
        PointsModel pointsModel = new PointsModel();
        BeanMapper.copy(pointsVO, pointsModel);
        pointsDao.save(pointsModel);
    }

    /**
     * 分页查询积分
     *
     * @return
     */
    @Override
    public List<PointsVO> listByPage(Integer pageNum, Integer pageSize) {
        List<PointsVO> pointsVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<PointsModel> pointsModels = pointsDao.listByPage();

        for (PointsModel pointsModel : pointsModels) {
            PointsVO pointsVO = new PointsVO();
            BeanMapper.copy(pointsModel, pointsVO);
            pointsVoS.add(pointsVO);
        }

        return pointsVoS;
    }

    /**
     * 根据用户id查询积分
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public PointsVO findByUserId(Integer userId) {
        PointsModel pointsModel = pointsDao.findByUserId(userId);
        PointsVO pointsVO = new PointsVO();
        BeanMapper.copy(pointsModel, pointsVO);
        return pointsVO;
    }
}
