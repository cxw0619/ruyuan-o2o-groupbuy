package com.ruyuan.o2o.groupbuy.image.service;

/**
 * 图片service组件接口
 *
 * @author ming qian
 */
public interface ImageService {
    /**
     * 上传图片
     *
     * @return 上传图片成功后返回的图片url地址
     */
    String uploadImage();
}
