package com.ruyuan.o2o.groupbuy.account.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.account.model.AuthModel;
import com.ruyuan.o2o.groupbuy.account.service.AuthService;
import com.ruyuan.o2o.groupbuy.account.vo.AuthVO;
import com.ruyuan.o2o.groupbuy.account.admin.dao.AuthDao;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理管理权限增删改查service组件
 *
 * @author ming qian
 */
@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthDao authDao;

    /**
     * 创建权限
     *
     * @param authVO
     */
    @Override
    public void save(AuthVO authVO) {
        AuthModel authModel = new AuthModel();
        BeanMapper.copy(authVO, authModel);
        authModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        authModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        authDao.save(authModel);
    }

    /**
     * 分页查询权限
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @Override
    public List<AuthVO> listByPage(Integer pageNum, Integer pageSize) {
        List<AuthVO> authVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<AuthModel> authModels = authDao.listByPage();
        for (AuthModel authModel : authModels) {
            AuthVO authVO = new AuthVO();
            BeanMapper.copy(authModel, authVO);
            authVoS.add(authVO);
        }

        return authVoS;
    }

}
