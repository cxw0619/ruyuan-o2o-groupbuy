package com.ruyuan.o2o.groupbuy.account.admin.service;

import com.ruyuan.o2o.groupbuy.account.model.AccountModel;
import com.ruyuan.o2o.groupbuy.account.service.LoginService;
import com.ruyuan.o2o.groupbuy.account.admin.dao.AccountDao;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 运营管理平台账号登录service组件
 *
 * @author ming qian
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AccountDao accountDao;

    /**
     * 运营管理平台用户登录
     *
     * @param adminName 管理员名称
     * @param passwd    管理员密码
     * @return
     */
    @Override
    public Boolean login(String adminName, String passwd) {

        AccountModel accountModel = new AccountModel();
        accountModel.setAdminName(adminName);
        accountModel.setPasswd(passwd);

        Integer closed = accountDao.findClosedByNameAndPasswd(accountModel);
        if (null == closed || closed.equals(1)) {
            //  当前用户已被冻结或不存在
            log.info("当前用户已被冻结或不存在:adminName:{}", adminName);
            return Boolean.FALSE;
        }
        //  执行登录，更新登录时间
        accountModel.setLastLogin(TimeUtil.format(System.currentTimeMillis()));
        accountDao.updateLastLogin(accountModel);
        log.info("更新登录完成");
        return Boolean.TRUE;
    }
}
