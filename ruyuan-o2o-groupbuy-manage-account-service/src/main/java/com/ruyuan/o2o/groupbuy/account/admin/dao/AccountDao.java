package com.ruyuan.o2o.groupbuy.account.admin.dao;

import com.ruyuan.o2o.groupbuy.account.model.AccountModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 账号服务DAO
 *
 * @author ming qian
 */
@Repository
public interface AccountDao {

    /**
     * 根据用户名称和用户密码查询用户冻结参数
     *
     * @param accountModel 平台用户model
     * @return closed参数
     */
    Integer findClosedByNameAndPasswd(AccountModel accountModel);

    /**
     * 更新账号最后登录时间
     *
     * @param accountModel 平台用户model
     */
    void updateLastLogin(AccountModel accountModel);

    /**
     * 创建账号
     *
     * @param accountModel 账号model
     */
    void save(AccountModel accountModel);

    /**
     * 更新账号
     *
     * @param accountModel 账号model
     */
    void update(AccountModel accountModel);

    /**
     * 冻结账号
     *
     * @param adminId 账号id
     */
    void close(Integer adminId);

    /**
     * 开启账号
     *
     * @param adminId 账号id
     */
    void open(Integer adminId);

    /**
     * 分页查询账号
     *
     * @return 账号列表
     */
    List<AccountModel> listByPage();

    /**
     * 根据id查询账号
     *
     * @param adminId 账号id
     * @return 账号model
     */
    AccountModel findById(Integer adminId);
}
