package com.ruyuan.o2o.groupbuy.evaluate.service;

import com.ruyuan.o2o.groupbuy.evaluate.model.EvaluateModel;
import com.ruyuan.o2o.groupbuy.evaluate.vo.EvaluateVO;

/**
 * 评价服务service组件接口
 *
 * @author ming qian
 */
public interface EvaluateService {
    /**
     * 用户评价
     *
     * @param evaluateVO
     */
    void save(EvaluateVO evaluateVO);

    /**
     * 根据商品id查询商品评价
     *
     * @param itemId 商品id
     * @return 评价model
     */
    EvaluateModel findByItemId(Integer itemId);
}
