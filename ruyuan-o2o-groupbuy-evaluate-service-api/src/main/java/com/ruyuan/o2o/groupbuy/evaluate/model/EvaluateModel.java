package com.ruyuan.o2o.groupbuy.evaluate.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 评价服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class EvaluateModel implements Serializable {
    private static final long serialVersionUID = -924469757684432073L;

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 消费者用户id
     */
    private Integer userId;
    /**
     * 门店id
     */
    private Integer storeId;
    /**
     * 商品id
     */
    private Integer itemId;
    /**
     * 评价等级 1 一星 2 二星 3 三星 4 四星 5 五星
     */
    private Integer level;
    /**
     * 创建时间
     */
    private String createTime;
}