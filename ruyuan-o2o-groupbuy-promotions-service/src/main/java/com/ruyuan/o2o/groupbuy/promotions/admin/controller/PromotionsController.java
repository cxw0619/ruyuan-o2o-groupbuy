package com.ruyuan.o2o.groupbuy.promotions.admin.controller;

import com.ruyuan.o2o.groupbuy.promotions.service.PromotionsService;
import com.ruyuan.o2o.groupbuy.promotions.vo.PromotionsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 活动管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/promotions")
@Slf4j
@Api(tags = "活动管理")
public class PromotionsController {

    @Reference(version = "1.0.0", interfaceClass = PromotionsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PromotionsService promotionsService;

    /**
     * 创建活动
     *
     * @param promotionsVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建活动")
    public Boolean save(@RequestBody PromotionsVO promotionsVO) throws Exception {
        promotionsService.save(promotionsVO);
        log.info("创建活动:{} 完成", promotionsVO.getPromotionName());
        return Boolean.TRUE;
    }

    /**
     * 更新活动
     *
     * @param promotionsVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新活动")
    public Boolean update(@RequestBody PromotionsVO promotionsVO) {
        promotionsService.update(promotionsVO);
        log.info("更新活动:{} 完成", promotionsVO.getPromotionName());
        return Boolean.TRUE;
    }


    /**
     * 分页查询活动列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询活动列表")
    public List<PromotionsVO> page(Integer pageNum, Integer pageSize) {
        List<PromotionsVO> promotionsVoS = promotionsService.listByPage(pageNum, pageSize);
        return promotionsVoS;
    }

    /**
     * 根据id查询活动
     *
     * @param promotionId 活动id
     * @return
     */
    @GetMapping("/{promotionId}")
    @ApiOperation("查询活动详情")
    public PromotionsVO page(@PathVariable("promotionId") Integer promotionId) {
        PromotionsVO promotionsVO = promotionsService.findById(promotionId);
        return promotionsVO;
    }

    /**
     * 删除活动
     *
     * @param promotionsVO 前台表单
     * @return
     */
    @PostMapping("/delete")
    @ApiOperation("删除活动")
    public Boolean delete(@RequestBody PromotionsVO promotionsVO) {
        promotionsService.delete(promotionsVO.getPromotionId());
        log.info("删除活动:{} 完成", promotionsVO.getPromotionName());
        return Boolean.TRUE;
    }
}
