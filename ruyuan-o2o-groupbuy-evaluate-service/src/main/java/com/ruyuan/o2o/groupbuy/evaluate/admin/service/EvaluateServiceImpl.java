package com.ruyuan.o2o.groupbuy.evaluate.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.evaluate.admin.dao.EvaluateDao;
import com.ruyuan.o2o.groupbuy.evaluate.model.EvaluateModel;
import com.ruyuan.o2o.groupbuy.evaluate.service.EvaluateService;
import com.ruyuan.o2o.groupbuy.evaluate.vo.EvaluateVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理评价服务service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = EvaluateService.class, cluster = "failfast", loadbalance = "roundrobin")
public class EvaluateServiceImpl implements EvaluateService {

    @Autowired
    private EvaluateDao evaluateDao;

    /**
     * 保存评价
     *
     * @param evaluateVO
     */
    @Override
    public void save(EvaluateVO evaluateVO) {
        EvaluateModel evaluateModel = new EvaluateModel();
        BeanMapper.copy(evaluateVO, evaluateModel);
        evaluateModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        evaluateDao.save(evaluateModel);
    }

    /**
     * 根据商品id查询商品评价
     *
     * @param itemId
     */
    @Override
    public EvaluateModel findByItemId(Integer itemId) {
        return evaluateDao.findByItemId(itemId);
    }
}
