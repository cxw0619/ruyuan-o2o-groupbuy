package com.ruyuan.o2o.groupbuy.code.admin.dao;

import com.ruyuan.o2o.groupbuy.code.model.CodeModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 券码服务DAO
 *
 * @author ming qian
 */
@Repository
public interface CodeDao {
    /**
     * 保存券码
     *
     * @param codeModel
     */
    void save(CodeModel codeModel);

    /**
     * 根据id查询券码
     *
     * @param userId 用户id
     * @param payId  支付id
     * @return
     */
    CodeModel findById(@Param("userId") Integer userId, @Param("payId") Integer payId);

    /**
     * 核销券码
     *
     * @param id         主键id
     * @param codeStatus 券码状态
     * @param updateTime 更新时间
     */
    void writeOffCode(@Param("id") Integer id, @Param("codeStatus") Integer codeStatus,
                      @Param("updateTime") String updateTime);
}