package com.ruyuan.o2o.groupbuy.code.http;

import com.ruyuan.o2o.groupbuy.code.service.CodeService;
import com.ruyuan.o2o.groupbuy.code.vo.CodeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

/**
 * 用户券码管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/code")
@Slf4j
@Api(tags = "用户券码管理")
public class CodeHttpController {

    @Reference(version = "1.0.0", interfaceClass = CodeService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private CodeService codeService;

    /**
     * 支付成功生成券码
     *
     * @param codeVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("生成券码")
    public Boolean save(@RequestBody CodeVO codeVO) {
        codeService.save(codeVO);
        log.info("生成券码完成");
        return Boolean.TRUE;
    }

    /**
     * 核销券码
     *
     * @param codeVO 前台表单
     * @return
     */
    @PostMapping("/write")
    @ApiOperation("核销券码")
    public Boolean writeOff(@RequestBody CodeVO codeVO) {
        codeService.writeOffCode(codeVO);
        log.info("核销券码完成");
        return Boolean.TRUE;
    }
}
