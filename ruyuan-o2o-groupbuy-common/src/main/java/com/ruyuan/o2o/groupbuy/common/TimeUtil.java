package com.ruyuan.o2o.groupbuy.common;

import java.text.SimpleDateFormat;

/**
 * 时间工具类
 *
 * @author ming qian
 */
public class TimeUtil {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static SimpleDateFormat sdf_excel = new SimpleDateFormat("yyyy-MM-dd");

    public static String format(Long time) {
        return sdf.format(time);
    }

    public static String formatExcel(Long time) {
        return sdf_excel.format(time);
    }
}
