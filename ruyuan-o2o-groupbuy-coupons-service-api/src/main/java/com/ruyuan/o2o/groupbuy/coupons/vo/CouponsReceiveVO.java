package com.ruyuan.o2o.groupbuy.coupons.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户领取优惠券VO类
 *
 * @author ming qian
 */
@ApiModel(value = "用户领取优惠券VO类")
@Getter
@Setter
@ToString
public class CouponsReceiveVO implements Serializable {
    private static final long serialVersionUID = 1074219178829678502L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 优惠券id
     */
    @ApiModelProperty("优惠券id")
    private Integer couponsId;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Integer userId;

    /**
     * 优惠券状态 0 未使用 1 已使用 2 已过期
     */
    @ApiModelProperty("优惠券状态 0 未使用 1 已使用 2 已过期")
    private Integer couponsStatus;

    /**
     * 领取时间
     */
    @ApiModelProperty("领取时间")
    private String receiveTime;
}