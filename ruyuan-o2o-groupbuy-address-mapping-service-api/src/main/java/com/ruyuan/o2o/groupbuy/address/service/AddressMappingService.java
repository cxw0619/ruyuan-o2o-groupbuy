package com.ruyuan.o2o.groupbuy.address.service;

import com.ruyuan.o2o.groupbuy.address.vo.AddressMappingVO;

import java.util.List;

/**
 * 省市区服务service组件接口
 *
 * @author ming qian
 */
public interface AddressMappingService {
    /**
     * 创建省市区映射
     *
     * @param addressMappingVO 映射vo
     * @return 创建结果
     */
    Boolean save(AddressMappingVO addressMappingVO);

    /**
     * 更新省市区映射
     *
     * @param addressMappingVO 映射vo
     */
    void update(AddressMappingVO addressMappingVO);

    /**
     * 分页查询省市区映射
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<AddressMappingVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 查询省名称
     *
     * @param provinceId 省id
     * @return
     */
    String findProvinceName(Integer provinceId);

    /**
     * 查询市名称
     *
     * @param cityId 市id
     * @return
     */
    String findCityName(Integer cityId);

    /**
     * 查询区名称
     *
     * @param districtId 区id
     * @return
     */
    String findDistrictName(Integer districtId);

}
