package com.ruyuan.o2o.groupbuy.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 立即购买VO类
 *
 * @author ming qian
 */
@ApiModel(value = "立即购买VO类")
@Getter
@Setter
@ToString
public class OrderBuyVO implements Serializable {
    private static final long serialVersionUID = -1031933400667134260L;
    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 消费者用户登录标记
     * true当前是登录状态
     */
    @ApiModelProperty("消费者用户登录标记")
    private Boolean isLogin;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 商户id
     */
    @ApiModelProperty("商户id")
    private Integer shopId;

    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Integer itemId;

    /**
     * 商品购买数量
     */
    @ApiModelProperty("商品购买数量")
    private Integer itemNum;

    /**
     * 活动id
     */
    @ApiModelProperty("活动id")
    private Integer promotionsId;

    /**
     * 优惠券id
     */
    @ApiModelProperty("优惠券id")
    private Integer couponsId;

    /**
     * 订单实际金额
     */
    @ApiModelProperty("订单实际金额")
    private Double orderPrice;

    /**
     * 订单类型 0 到店消费 1配送
     */
    @ApiModelProperty("订单类型 0 到店消费 1配送")
    private Integer orderType;

}