package com.ruyuan.o2o.groupbuy.shop.admin.dao;

import com.ruyuan.o2o.groupbuy.shop.model.ShopStoreModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 商户服务DAO
 *
 * @author ming qian
 */
@Repository
public interface ShopDao {

    /**
     * 创建门店
     *
     * @param shopStoreModel
     */
    void save(ShopStoreModel shopStoreModel);

    /**
     * 更新门店
     *
     * @param shopStoreModel
     */
    void update(ShopStoreModel shopStoreModel);

    /**
     * 下架门店
     *
     * @param storeId
     */
    void offLine(Integer storeId);

    /**
     * 上架门店
     *
     * @param storeId
     */
    void onLine(Integer storeId);

    /**
     * 分页查询门店
     *
     * @return
     */
    List<ShopStoreModel> listByPage();

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return
     */
    ShopStoreModel findById(Integer storeId);

    /**
     * 删除门店
     *
     * @param storeId
     */
    void delete(Integer storeId);

}
