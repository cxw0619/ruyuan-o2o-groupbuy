package com.ruyuan.o2o.groupbuy.shop.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.address.service.AddressMappingService;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.shop.admin.dao.ShopDao;
import com.ruyuan.o2o.groupbuy.shop.model.ShopStoreModel;
import com.ruyuan.o2o.groupbuy.shop.service.ShopService;
import com.ruyuan.o2o.groupbuy.shop.vo.ShopStoreVO;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理商户服务操作门店的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ShopService.class, cluster = "failfast", loadbalance = "roundrobin")
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopDao shopDao;

    @Reference(version = "1.0.0", interfaceClass = AddressMappingService.class, cluster = "failfast", loadbalance = "roundrobin")
    private AddressMappingService addressMappingService;

    /**
     * 创建门店
     *
     * @param shopStoreVO
     */
    @Override
    public Boolean save(ShopStoreVO shopStoreVO) {
        ShopStoreModel shopStoreModel = new ShopStoreModel();
        BeanMapper.copy(shopStoreVO, shopStoreModel);

        String provinceName = addressMappingService.findProvinceName(shopStoreModel.getProvinceId());
        String cityName = addressMappingService.findCityName(shopStoreModel.getCityId());
        String districtName = addressMappingService.findDistrictName(shopStoreModel.getDistrictId());
        String storeAddress = provinceName + cityName + districtName + shopStoreModel.getStoreAddress();

        shopStoreModel.setStoreAddress(storeAddress);
        shopStoreModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        shopStoreModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));

        shopDao.save(shopStoreModel);
        return Boolean.TRUE;
    }

    /**
     * 更新门店
     *
     * @param shopStoreVO
     */
    @Override
    public Boolean update(ShopStoreVO shopStoreVO) {
        ShopStoreModel shopStoreModel = new ShopStoreModel();
        BeanMapper.copy(shopStoreVO, shopStoreModel);
        shopStoreModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        shopDao.update(shopStoreModel);
        return Boolean.TRUE;
    }

    /**
     * 下架门店
     *
     * @param storeId
     */
    @Override
    public Boolean offLine(Integer storeId) {
        shopDao.offLine(storeId);
        return Boolean.TRUE;
    }

    /**
     * 上架门店
     *
     * @param storeId
     */
    @Override
    public Boolean onLine(Integer storeId) {
        shopDao.onLine(storeId);
        return Boolean.TRUE;
    }

    /**
     * 分页查询门店
     *
     * @return
     */
    @Override
    public List<ShopStoreVO> listByPage(Integer pageNum, Integer pageSize) {
        List<ShopStoreVO> shopStoreVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<ShopStoreModel> shopStoreModels = shopDao.listByPage();

        for (ShopStoreModel shopStoreModel : shopStoreModels) {
            ShopStoreVO shopStoreVO = new ShopStoreVO();
            BeanMapper.copy(shopStoreModel, shopStoreVO);
            shopStoreVoS.add(shopStoreVO);
        }

        return shopStoreVoS;
    }

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return
     */
    @Override
    public ShopStoreVO findById(Integer storeId) {
        ShopStoreModel shopStoreModel = shopDao.findById(storeId);
        ShopStoreVO shopStoreVO = new ShopStoreVO();
        BeanMapper.copy(shopStoreModel, shopStoreVO);
        return shopStoreVO;
    }

    /**
     * 删除门店
     *
     * @param storeId
     */
    @Override
    public Boolean delete(Integer storeId) {
        shopDao.delete(storeId);
        return Boolean.TRUE;
    }
}
